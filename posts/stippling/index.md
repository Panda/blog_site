<!--
.. title: Stippling.
.. slug: stippling
.. date: 2020-07-15 01:03:53 UTC+02:00
.. tags: stippling, voronoi
.. category: stippling
.. link:
.. description: Stippling.
.. type: text
.. has_math: true
.. previewimage: /images/stippling/stippling.png
.. status: featured
-->

Stippling is the art of harmoniously placing "stipples", basically dots, to form an image.

This method can produce beautiful portraits or sceneries.

I use Linde-Buzo-Gray Weighted stippling method. It makes use of the previously developped Voronoï diagrams !

<center><img src="/images/stippling/bruges.png" alt="A stippling." width="800"/></center>

The other ones are drawn using a pen plotter !
<!-- TEASER_END -->

<div class="row">
  <div class="column">
    <img src="/images/stippling/macaw.png">
    <img src="/images/stippling/dandelion.png">
    <img src="/images/stippling/coffee_beans.png">
    <img src="/images/stippling/waiting.png">

  </div>
  <div class="column">
    <img src="/images/stippling/portrait_glasses.png">
    <img src="/images/stippling/buildings.png">
    <img src="/images/stippling/moon_cat.png">
  </div>
</div>
