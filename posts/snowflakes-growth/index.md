<!--
.. title: Snowflakes growth.
.. slug: snowflakes-growth
.. date: 2020-09-10 23:18:53 UTC+02:00
.. tags: snowflakes
.. category: snowflakes
.. link:
.. description: Growing snowflakes at home.
.. type: text
.. has_math: true
.. previewimage: /images/snowflakes/snowflake.png
.. status: featured
-->

I have a deep love for all snowflakes. I often open a book, "Kenneth Libbrecht - The Art of the Snowflake, A Photographic Album" to see how complex they are. So when I found a paper on how to model snowflake growth, I had to implement it.

Modeling snow-crystal growth: A three-dimensional mesoscopic approach, by Janko Gravner and David Griffeath.

<center><img src="/images/snowflakes/snowflake7.png" alt="A snowflake." width="800"/></center>

They are drawn using POV-Ray.
<!-- TEASER_END -->

<center><img src="/images/snowflakes/snowflake.png" alt="A snowflake." width="800"/></center>
<center><img src="/images/snowflakes/snowflake2.png" alt="A snowflake." width="800"/></center>
<center><img src="/images/snowflakes/snowflake3.png" alt="A snowflake." width="800"/></center>
<center><img src="/images/snowflakes/snowflake4.png" alt="A snowflake." width="800"/></center>
<center><img src="/images/snowflakes/snowflake5.png" alt="A snowflake." width="800"/></center>
<center><img src="/images/snowflakes/snowflake6.png" alt="A snowflake." width="800"/></center>
<center><img src="/images/snowflakes/snowflake9.png" alt="A snowflake." width="800"/></center>
