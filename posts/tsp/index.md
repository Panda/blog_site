<!--
.. title: Traveling Salesman Problem.
.. slug: tsp
.. date: 2020-08-01 11:16:53 UTC+02:00
.. tags: tsp, voronoi, stippling
.. category: tsp
.. link:
.. description: TSP.
.. type: text
.. has_math: true
.. status: featured
-->

TSP stands for "Traveling Salesman Problem". It tells the story of a salesman willing to find the shortest path between all the cities he travels to, to sell his goods. Here, we draw the path he travels to reveal an image with meandering lines.

<center><img src="/images/tsp/tsp_mst_50000.png" alt="A TSP art." width="800"/></center>
They are all drawn using a pen plotter !
<!-- TEASER_END -->

<div class="row">
  <div class="column">
    <img src="/images/tsp/macaw.png">
    <img src="/images/tsp/street_fashion.jpg">


  </div>
  <div class="column">
    <img src="/images/tsp/portrait_blue.png">
    <img src="/images/tsp/cat_eyes.png">
    <img src="/images/tsp/portrait_dark_purple.png">
  </div>
</div>


Most of the original images come from [Pixabay](https://pixabay.com).
