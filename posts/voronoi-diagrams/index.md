<!--
.. title: Voronoi diagrams.
.. slug: voronoi-diagrams
.. date: 2020-06-27 11:16:53 UTC+02:00
.. tags: voronoi
.. category: voronoi
.. link:
.. description: Voronoi diagrams.
.. type: text
.. has_math: true
.. status: featured
-->


It took me some times, but I wanted to implement Fortune's algorithm to produce the Voronoï Diagram of a set of points in 2D, and I finally did. I originally thought of Voronoï diagram as a mathematical tool, but now I think that these diagrams can be beautiful by themselves. Take a look at the progression !

<center><img src="/images/voronoi/01_voronoi_construction.gif" alt="A voronoi diagram with points added." width="400"/></center>

<!-- TEASER_END -->

I can print diagrams in SVG files with quite a lot of features. The first gif above was an ugly beginning but now I can produce beautiful ones ! Let's add some colors. And well placed sites.

## Geometry inspired diagrams
<center><img src="/images/voronoi/22_5symmetry.png" alt="A voronoi diagram." width="400"/></center>

<center><img src="/images/voronoi/26_circle.png" alt="A voronoi diagram." width="400"/></center>

<center><img src="/images/voronoi/24_cardioid.png" alt="A voronoi diagram." width="400"/></center>

<center><img src="/images/voronoi/24_heart.png" alt="A voronoi diagram." width="400"/></center>

<center><img src="/images/voronoi/27_autumn_gradient.png" alt="A voronoi diagram." width="400"/></center>

## With an input image and a little work...
<center><img src="/images/voronoi/32_papillon.png" alt="A voronoi diagram." width="400"/></center>

<center><img src="/images/voronoi/19_joconde.png" alt="A voronoi diagram." width="400"/></center>

## Flowers
This is what I call Voronoi flowers :
<div class="row">
  <div class="column">
  <img src="/images/voronoi_flowers/vf3.svg"  width="400">
  <img src="/images/voronoi_flowers/vf5.svg"  width="400">
  <img src="/images/voronoi_flowers/vf15.svg"  width="400">
  <img src="/images/voronoi_flowers/vf19.svg"  width="400">
  <img src="/images/voronoi_flowers/vf36.svg"  width="400">
  <img src="/images/voronoi_flowers/vf43.svg"  width="400">
  <img src="/images/voronoi_flowers/vf44.svg"  width="400">
  <img src="/images/voronoi_flowers/vf58.svg"  width="400">
  <img src="/images/voronoi_flowers/vf68.svg"  width="400">
  <img src="/images/voronoi_flowers/vf75.svg"  width="400">
  <img src="/images/voronoi_flowers/vf116.svg"  width="400">
  <img src="/images/voronoi_flowers/vf127.svg"  width="400">
  <img src="/images/voronoi_flowers/vf147.svg"  width="400">

  </div>
  <div class="column">
  <img src="/images/voronoi_flowers/vf192.svg"  width="400">
  <img src="/images/voronoi_flowers/vf211.svg"  width="400">
  <img src="/images/voronoi_flowers/vf220.svg"  width="400">
  <img src="/images/voronoi_flowers/vf241.svg"  width="400">
  <img src="/images/voronoi_flowers/vf242.svg"  width="400">
  <img src="/images/voronoi_flowers/vf286.svg"  width="400">
  <img src="/images/voronoi_flowers/vf318.svg"  width="400">
  <img src="/images/voronoi_flowers/vf325.svg"  width="400">
  <img src="/images/voronoi_flowers/vf329.svg"  width="400">
  <img src="/images/voronoi_flowers/vf348.svg"  width="400">
  <img src="/images/voronoi_flowers/vf362.svg"  width="400">
  <img src="/images/voronoi_flowers/vf397.svg"  width="400">
  </div>
</div>


## Snowflakes
This is what I call Voronoi Snowflakes :
<div class="row">
  <div class="column">
  <img src="/images/voronoi_snowflakes/vs13.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs24.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs52.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs68.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs75.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs77.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs83.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs85.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs95.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs135.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs150.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs152.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs168.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs170.svg"  width="400">
  </div>
  <div class="column">
  <img src="/images/voronoi_snowflakes/vs181.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs181.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs187.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs203.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs217.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs220.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs230.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs251.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs265.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs276.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs279.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs282.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs295.svg"  width="400">
  <img src="/images/voronoi_snowflakes/vs299.svg"  width="400">
  </div>
</div>
