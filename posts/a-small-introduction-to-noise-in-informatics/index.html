<!DOCTYPE html>
<html prefix="
" lang="en">
<head>
<meta charset="utf-8">
<meta name="description" content="What are noise functions ? Why do we use them ?">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Ondines' dev blog · A small introduction to noise in informatics. </title>
<link href="../../assets/css/all-nocdn.css" rel="stylesheet" type="text/css">
<link href="../../assets/css/ipython.min.css" rel="stylesheet" type="text/css">
<link href="../../assets/css/nikola_ipython.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700%7CAbril+Fatface">
<meta content="#001010" name="theme-color">
<link rel="alternate" type="application/rss+xml" title="RSS" href="../../rss.xml">
<link rel="canonical" href="https://ondin.es/posts/a-small-introduction-to-noise-in-informatics/">
<!--[if lt IE 9]><script src="../../assets/js/html5.js"></script><![endif]--><meta name="author" content="">
<link rel="next" href="../a-first-noise-the-value-noise/" title="A first noise : the Value Noise" type="text/html">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.2/dist/katex.min.css" integrity="sha384-yFRtMMDnQtDRO8rLpMIKrtPCD5jdktao2TV19YiZYWMDkUR5GQZR/NOVTdquEx1j" crossorigin="anonymous">
</head>
<body class="theme-base-0b">
    <a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>

    <div class="hsidebar">
        <div class="container sidebar-sticky">
            <div class="sidebar-about">
              <h1>
                <a href="https://ondin.es/">
                      <h1 id="brand"><a href="https://ondin.es/" title="Ondines' dev blog" rel="home">

        <span id="blog-title">Ondines' dev blog</span>
    </a></h1>

                </a>
              </h1>
                <p class="lead">Just a wee blog where I talk about the things I'm doing.</p>

            </div>
                <nav id="menu" role="navigation" class="sidebar-nav"><a class="sidebar-nav-item" href="../../archive.html">Archive</a>
        <a class="sidebar-nav-item" href="../../categories/">Tags</a>
        <a class="sidebar-nav-item" href="../../rss.xml">RSS feed</a>
        <a class="sidebar-nav-item" href="../../galleries/">Beautiful Pictures</a>
    
    
    </nav><footer id="footer"><span class="copyright">
              Contents © 2021         <a href="mailto:nausicaa@ondin.es"></a> - Powered by         <a href="https://getnikola.com" rel="nofollow">Nikola</a>         
            </span>
            
            
        </footer>
</div>
    </div>

    <div class="content container" id="content">
<article class="post-text h-entry hentry postpage" itemscope="itemscope" itemtype="http://schema.org/Article"><h1 class="post-title p-name"><a href="." class="u-url">A small introduction to noise in informatics.</a></h1>

    <span class="post-date">
      <time class="published dt-published" datetime="2020-04-24T18:16:55+02:00" itemprop="datePublished" title="2020-04-24 18:16">2020-04-24 18:16</time></span>
        <meta name="description" itemprop="description" content="What are noise functions ? Why do we use them ?">
<div class="e-content entry-content" itemprop="articleBody text">
    <div>
<h2>Introduction</h2>
<p>Just a small blog post to introduce the notion of noise, used particularly in texture synthesis for natural phenomenons. For this introduction, no need to have a mathematical background. In the next posts, I'll try to explain the maths behind noise functions, but that's for later.</p>
<!-- TEASER_END -->

<h2>Noise ?</h2>
<p>The first time I heard about <em>noise</em> in a mathematical context was in a signal processing class. Basically, noise is everything unwanted. You want to record audio ? Noise will be everything that you didn't want to hear with your microphone ; it will be what your microphone gives when you think everything is silent. The name "noise" comes from acoustics and radio signals : you really want to send some <em>information</em>, but what is received is a little bit different. It's <em>information</em>, yes, but with <em>noise</em>. Your crystal-clear voice can become something almost inaudible. By extension, in signal processing, "noise" is the name given to everything that is not the information you wanted to send.</p>
<p>Often, noise is not in our beautiful, idealized mathematical models of informations. Noise comes from <em>reality</em>. From imperfect physical phenomenons that we will never be able to control exactly.</p>
<p></p>
<center><img src="../../images/noise/signal_and_reality.png" alt="Left : theoretical signal you want to send. Right : the same signal with noise. Probably the signal that you send in reality." width="800"></center>
<h6>
<em>Left</em> : theoretical signal you want to send. <em>Right</em> : the same information, but with noise. It's probably the signal that you send in reality.</h6>
<p>Therefore, we study a lot noise and particularly noise reduction algorithm in signal processing. We want to get rid of that extra bit of unwanted information. It's just some random <em>garbage</em>, really.</p>
<p>One exception, though, and that's what will interest us here, is when you want to mimic <em>reality</em>. Reality is full of noise. Nothing is perfect. There is what we call "randomness" everywhere in nature. Let's take an example. You want to simulate a realistic, natural landscape. You will need noise. Have you ever seen a landscape like that in nature ?</p>
<p></p>
<center><img src="../../images/noise/mathematical_landscape.png" alt="A sinusoïdal landscape. Something that doesn't exist in nature." width="800"></center>
<h6>A sinusoïdal landscape. Something that doesn't exist in nature.</h6>
<p>Of course not. You probably want more randomness to your landscape. Sometimes you'll want high mountains, hills, or sometimes lakes, maybe, but without a visible pattern of repeating ups and downs like the previous example. Take a look at this one :</p>
<p></p>
<center><img src="../../images/noise/natural_landscape.png" alt="A somewhat more natural landscape. Something that could exist in nature." width="800"></center>
<h6>A somewhat more natural landscape. Something that could exist in nature.</h6>
<p>Isn't it better ? Isn't it a more <em>believable</em> landscape ? Notice however that the randomness in this landscape is quite special. It's not completely random. A completely random landscape would look like this :</p>
<p></p>
<center><img src="../../images/noise/random_landscape.png" alt="A completely random landscape. It's not natural at all, but in a different way than the mathematical idealization of a landscape." width="800"></center>
<h6>A completely random landscape. It's not natural at all, but in a different way than the mathematical idealization of a landscape.</h6>
<p>We see that we need a <strong>coherent noise</strong>. If we have the peak of a mountain at one point, there's no chance to find a deep sea nearby. We have first to walk down the mountain. It takes some times. Here, <em>coherent</em> means "continuous", "smooth". The height at one point is correlated to the height of neighbour points.</p>
<p>One last point. Many things in nature are fractal. If you don't know what does that mean, don't worry. Just think of "something with various levels of details". To continue with landscape, at a low level of details, we can consider only mountains and big variations in height.</p>
<p></p>
<center><img src="../../images/noise/fractal_landscape0.png" alt="Fractal construction of a landscape. First, big variations." width="400"></center>
<h6>Fractal construction of a landscape. First, big variations.</h6>
<p>Then we can consider hills, which are medium variations of the landscape.</p>
<p></p>
<center><img src="../../images/noise/fractal_landscape1.png" alt="Fractal construction of a landscape. Second, medium variations." width="400"></center>
<h6>Fractal construction of a landscape. Second, medium variations.</h6>
<p>But we'll also have boulders that locally will change the height.</p>
<p></p>
<center><img src="../../images/noise/fractal_landscape2.png" alt="Fractal construction of a landscape. Third, small variations." width="400"></center>
<h6>Fractal construction of a landscape. Third, small variations.</h6>
<p>But if we zoom in, the stones will also give small variations :</p>
<p></p>
<center><img src="../../images/noise/fractal_landscape3.png" alt="Fractal construction of a landscape. Finally, very small variations." width="800"></center>
<h6>Fractal construction of a landscape. Finally, very small variations.</h6>
<p>And we could go on and on...</p>
<h2>Our goal with noise</h2>
<p>In this series of articles, we'll design noise algorithms in the Rust programming language. Our algorithms will share some specificities :</p>
<ul>
<li>they will be <strong>random</strong> at a large scale, however they will produce a <strong>coherent noise</strong>, a very special kind of randomness that is smooth and continous,</li>
<li>they will be <strong>reproducible</strong> : with the same input, we'll always give the same noise,</li>
<li>they will have the possibility to give an output with various levels of details (<strong>fractal noise</strong>),</li>
<li>they will be able to produce noise in 1, 2, 3 and 4 dimensions.</li>
<li>they will give a random value between -1 and 1.</li>
</ul>
<p>These noise algorithms have several applications in procedural content generation. We can make landscapes, clouds, fire, water surfaces, smooth animations... The possibilities are infinite, really.
For example, a 4D noise can simulate a 3D cloud smoothly animated, considering the time to be the fourth dimension.</p>
<p>In the <a href="../a-first-noise-the-value-noise/">next post</a>, we'll make a <em>value noise</em>. They are probably the simplest kind of noise and a very good opportunity to present the basic concepts of noise generation that will be shared by all our noise algorithms.</p>
<hr>
</div>
    </div>
    <aside class="postpromonav"><nav><p itemprop="keywords" class="tags">
            <span class="tag"><a class="p-category" href="../../categories/informatics/" rel="tag">informatics</a></span>
            <span class="tag"><a class="p-category" href="../../categories/introduction/" rel="tag">introduction</a></span>
            <span class="tag"><a class="p-category" href="../../categories/noise/" rel="tag">noise</a></span>
            <span class="tag"><a class="p-category" href="../../categories/procedural-content-generation/" rel="tag">procedural content generation</a></span>
      </p>

            <div class="pager hidden-print pagination">

            <span class="previous pagination-item older">
                Previous post
            </span>


            <span class="next pagination-item newer">
                <a href="../a-first-noise-the-value-noise/" rel="next" title="A first noise : the Value Noise">
Next post
              </a>
            </span>

        </div>

    </nav></aside><script src="https://cdn.jsdelivr.net/npm/katex@0.10.2/dist/katex.min.js" integrity="sha384-9Nhn55MVVN0/4OFx7EE5kpFBPsEMZxKTCnA+4fqDmg12eCTqGi6+BB2LjY8brQxJ" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/katex@0.10.2/dist/contrib/auto-render.min.js" integrity="sha384-kWPLUVMOks5AQFrykwIup5lo0m3iMkkHrD0uJ4H5cjeGihAutqP0yW0J6dpFiVkI" crossorigin="anonymous"></script><script>
                renderMathInElement(document.body,
                    {
                        delimiters: [
                            {left: "$$", right: "$$", display: true},
                            {left: "\\[", right: "\\]", display: true},
                            {left: "\\begin{equation*}", right: "\\end{equation*}", display: true},
                            {left: "\\(", right: "\\)", display: false}
                        ]
                    }
                );
            </script></article>
</div>
            <script src="../../assets/js/all-nocdn.js"></script><script>
    baguetteBox.run('div#content', {
        ignoreClass: 'islink',
        captions: function(element) {
            return element.getElementsByTagName('img')[0].alt;
    }});
    </script>
</body>
</html>
