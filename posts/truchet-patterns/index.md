<!--
.. title: Truchet Patterns.
.. slug: truchet-patterns
.. date: 2020-05-07 01:51:53 UTC+02:00
.. tags: truchet,truchet patterns,patterns
.. category: miscellaneous
.. link:
.. description: Presentation of Truchet Patterns.
.. type: text
.. has_math: true
.. status: featured
-->


Jumping on a new project today. Browsing the internet the other day, I found a paper about [Multi-scale Truchet Patterns](http://archive.bridgesmathart.org/2018/bridges2018-39.pdf) and wanted to give it a try.

Truchet Patterns are made of tiles that can be gathered even at different scales to show a beautiful pattern.

I wanted to try my new animation code so here is an animation showing how rich Truchet patterns can be, even with the same tiles, when rotating them.

<video width="500" height="500" controls autoplay loop>
 <source src="/images/truchet/truchet.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
