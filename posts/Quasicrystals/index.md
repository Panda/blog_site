<!--
.. title: Quasicrystals
.. slug: Quasicrystals
.. date: 2020-09-29 21:33:53 UTC+02:00
.. tags: Quasicrystals
.. category: Quasicrystals
.. link:
.. description: Quasicrystals.
.. type: text
.. has_math: true
.. status: featured
-->

Quasicrystals... Mysterious entities... a structure that is ordered but not periodic. A quasicrystalline pattern can continuously fill all available space, but it lacks translational symmetry.

This is an attempt to mimic them... With animation ! The symmetry seems to be everywhere but really is nowhere...


<video width="500" height="500" controls autoplay loop>
 <source src="/images/quasicrystals/qc.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>

<video width="500" height="500" controls autoplay loop>
 <source src="/images/quasicrystals/qc2.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
