<!--
.. title: Wallpaper groups.
.. slug: wallpaper-groups
.. date: 2020-05-29 11:16:53 UTC+02:00
.. tags: wallpaper
.. category: wallpapers
.. link:
.. description: The 17 wallpaper groups.
.. type: text
.. has_math: true
.. previewimage: /images/wallpaper/p3.png
.. status: featured
-->


A wallpaper group is a mathematical classification of a two-dimensional repetitive pattern, based on the symmetries in the pattern. There is 17 wallpaper groups. I made an example of each one with a domain coloring technique. I'd like to thank Frank A. Farris for his amazing book "Creating Symmetry".

I'll first show the 17 groups, then how we can play with them by morphing them or animating them.



# The 17 groups

## P1
<center><img src="/images/wallpaper/p1.png" alt="P1." width="400"/></center>


<!-- TEASER_END -->


## Pm
<center><img src="/images/wallpaper/pm.png" alt="Pm." width="400"/></center>

## Pg
<center><img src="/images/wallpaper/pg.png" alt="Pg." width="400"/></center>

## Pmm
<center><img src="/images/wallpaper/pmm.png" alt="Pmm." width="400"/></center>

## Pmg
<center><img src="/images/wallpaper/pmg.png" alt="Pmg." width="400"/></center>

## Pgg
<center><img src="/images/wallpaper/pgg.png" alt="Pgg." width="400"/></center>

## Cm
<center><img src="/images/wallpaper/cm.png" alt="cm." width="400"/></center>

## Cmm
<center><img src="/images/wallpaper/cmm.png" alt="cmm." width="400"/></center>

## P2
<center><img src="/images/wallpaper/p2.png" alt="P2." width="400"/></center>

## P3
<center><img src="/images/wallpaper/p3.png" alt="P3." width="400"/></center>

## P31m
<center><img src="/images/wallpaper/p31m.png" alt="P31m." width="400"/></center>

## P3m1
<center><img src="/images/wallpaper/p3m1.png" alt="P3m1." width="400"/></center>

## P4
<center><img src="/images/wallpaper/p4.png" alt="P4." width="400"/></center>

## P4m
<center><img src="/images/wallpaper/p4m.png" alt="P4m." width="400"/></center>

## P4g
<center><img src="/images/wallpaper/p4g.png" alt="P4g." width="400"/></center>

## P6
<center><img src="/images/wallpaper/p6.png" alt="P6." width="400"/></center>

## P6m
<center><img src="/images/wallpaper/p6m.png" alt="P6m." width="400"/></center>

# Morphing wallpapers
<center><img src="/images/wallpaper/morphingp6m.png" alt="morphing P6m." width="400"/></center>
<center><img src="/images/wallpaper/morphingp4.png" alt="morphing P4." width="400"/></center>
<center><img src="/images/wallpaper/morphingp4g.png" alt="morphing P4g." width="400"/></center>
<center><img src="/images/wallpaper/morphingp31m.png" alt="morphing P31m." width="400"/></center>
<center><img src="/images/wallpaper/morphingp3m1.png" alt="morphing P3m1." width="400"/></center>
<center><img src="/images/wallpaper/morphingp3.png" alt="morphing P3" width="400"/></center>

# Animated wallpapers
<video width="500" height="500" controls autoplay loop>
 <source src="/images/wallpaper/wallpaper.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
