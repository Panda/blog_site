<!--
.. title: Spirograph.
.. slug: spirograph
.. date: 2020-08-17 13:12:53 UTC+02:00
.. tags: spirograph
.. category: spirograph
.. link:
.. description: Spirograph.
.. type: text
.. has_math: true
.. status: featured
-->

I added a motion blur feature to my animation library and I just wanted to show you what it looks like with a spirograph animation !

<video width="500" height="500" controls autoplay loop>
 <source src="/images/spirograph/spirograph.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
