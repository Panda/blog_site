<!--
.. title: Understanding the Perlin Noise
.. slug: understanding-the-perlin-noise
.. date: 2020-05-03 02:09:09 UTC+02:00
.. tags: noise,perlin noise,informatics
.. category: noise
.. link:
.. description: Presentation of the Perlin Noise
.. type: text
.. has_math: true
.. previewimage: /images/noise/intro_preview.png
.. status: featured
-->

# Introduction

Sure, the value noise we've seen in the previous posts is nice, but it's very blocky, especially with one octave only :

<center><img src="/images/noise/value_noise_2D.png" alt="A sample of 2D value noise." width="400"/></center>
##### A sample of 2D value noise.

And that's very understandable : we fixed a grid of values and interpolates between these values, so we really do see the grid. That might be OK for some applications, but if you want to look natural, these alignments don't look good.

Perlin noise resulted from the work of Ken Perlin, who developed it for the movie Tron (1982) to improve the value noise.

To obfuscate the grid, the idea is not to associate a value at integers coordinates, but a *direction*.

We'll develop here a modern version of Perlin Noise in 1D, 2D, 3D and 4D.

<!-- TEASER_END -->

# Gradient Noise

Perlin Noise is categorized among the *gradient noises*. It's a class of noises that deals with *gradients*, meaning here *directions*, instead of values.

In this part, we'll try to forge an intuition of how Perlin Noise works in 1D and 2D.
