<!--
.. title: Agates.
.. slug: agates
.. date: 2020-10-01 17:10:53 UTC+02:00
.. tags: agates
.. category: agates
.. link:
.. description: agates.
.. type: text
.. has_math: true
.. previewimage: /images/agates/agates.png
.. status: featured
-->


My oldest project, first in Python, now in Rust : agates ! Mineral formations that I try to mimic here. It uses morphological operators to grow an agate.


<center><img src="/images/agates/agate.png" alt="An agate." width="800"/></center>
<center><img src="/images/agates/agate2.png" alt="An agate." width="800"/></center>
